<?php

class Article extends CI_Controller {
    
    function __construct() {
        parent::__construct();
		$this->load->library('form_validation');		
		$this->load->helper(array('form','url','codegen_helper'));
		$this->load->model('codegen_model','',TRUE);
	}	
	
	function index(){
		$this->manage();
	}

	function manage(){
        $this->load->library('table');
        $this->load->library('pagination');
        
        //paging
        $config['base_url'] = base_url().'index.php/article/manage/';
        $config['total_rows'] = $this->codegen_model->count('article');
        $config['per_page'] = 3;	
        $this->pagination->initialize($config); 	
        // make sure to put the primarykey first when selecting , 
		//eg. 'userID,name as Name , lastname as Last_Name' , Name and Last_Name will be use as table header.
		// Last_Name will be converted into Last Name using humanize() function, under inflector helper of the CI core.
		$this->data['results'] = $this->codegen_model->get('article','id,category_id,title,content,upload,author_id,created_date','',$config['per_page'],$this->uri->segment(3));
       
	   $this->load->view('article_list', $this->data); 
       //$this->template->load('content', 'article_list', $this->data); // if have template library , http://maestric.com/doc/php/codeigniter_template
		
    }
	
    function add(){        
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('article') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        } else
        {                            
            $data = array(
                    'category_id' => set_value('category_id'),
					'title' => set_value('title'),
					'content' => set_value('content'),
					'upload' => set_value('upload'),
					'author_id' => set_value('author_id'),
					'created_date' => set_value('created_date')
            );
           
			if ($this->codegen_model->add('article',$data) == TRUE)
			{
				//$this->data['custom_error'] = '<div class="form_ok"><p>Added</p></div>';
				// or redirect
				redirect(base_url().'index.php/article/manage/');
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>An Error Occured.</p></div>';

			}
		}		   
		$this->load->view('article_add', $this->data);   
        //$this->template->load('content', 'article_add', $this->data);
    }	
    
    function edit(){        
        $this->load->library('form_validation');    
		$this->data['custom_error'] = '';
		
        if ($this->form_validation->run('article') == false)
        {
             $this->data['custom_error'] = (validation_errors() ? '<div class="form_error">'.validation_errors().'</div>' : false);

        } else
        {                            
            $data = array(
                    'category_id' => $this->input->post('category_id'),
					'title' => $this->input->post('title'),
					'content' => $this->input->post('content'),
					'upload' => $this->input->post('upload'),
					'author_id' => $this->input->post('author_id'),
					'created_date' => $this->input->post('created_date')
            );
           
			if ($this->codegen_model->edit('article',$data,'id',$this->input->post('id')) == TRUE)
			{
				redirect(base_url().'index.php/article/manage/');
			}
			else
			{
				$this->data['custom_error'] = '<div class="form_error"><p>An Error Occured</p></div>';

			}
		}

		$this->data['result'] = $this->codegen_model->get('article','id,category_id,title,content,upload,author_id,created_date','id = '.$this->uri->segment(3),NULL,NULL,true);
		
		$this->load->view('article_edit', $this->data);		
        //$this->template->load('content', 'article_edit', $this->data);
    }
	
    function delete(){
            $ID =  $this->uri->segment(3);
            $this->codegen_model->delete('article','id',$ID);             
            redirect(base_url().'index.php/article/manage/');
    }
}

/* End of file article.php */
/* Location: ./system/application/controllers/article.php */